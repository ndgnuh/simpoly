#!/bin/sh
distributions="
cp37-cp37
cp38-cp38
cp39-cp39
cp310-cp310
cp311-cp311
cp312-cp312
pp37-pypy37_pp73
pp38-pypy38_pp73
pp39-pypy39_pp73
pp310-pypy310_pp73
"
distributions=$(echo $distributions | tr '\n' ' ')
manylinux-interpreters ensure $distributions
for python_ver in $distributions; do
    echo "Building for python $python_ver"
    bin_path="/opt/python/$python_ver/bin/"
    $bin_path/pip install setuptools --upgrade
    $bin_path/python3 -m build
done

echo "Using auditwheel to create many linux"
auditwheel repair dist/*.whl
