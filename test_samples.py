import math
import pyximport
pyximport.install()
import simpoly

polygon = [[0, 0], [-3, -4], [4, 1]]
eq = simpoly.naive_eq

def test_transforms():
    # TODO: more test
    simpoly.rotate(polygon, 0.2)
    polygon2 = simpoly.translate(polygon, 1, 1)
    polygon3 = simpoly.affine_transform(polygon, tuple([1, 0, 1, 0, 1, 1]))
    assert eq(polygon2, polygon3)
